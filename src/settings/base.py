"""Basic settings of the project.
Contains all configurations for the projectself.
Should NOT contain any secrets.
>>> import settings
>>> settings.DATA_DIR
"""
import os
from os import path
import sys

# Utils for the structure of the volatility surface
true_alpha = 0.1
true_beta = 0.1
true_sigma0 = 0.2

# Utils for the Black-Scholes Model
risk_free_rate = 0.05
annual_dividend_yield = 0

# Utils for the data
N = 10000

# DataFrame
CALL = 'Call Price'

MONEYNESS = 'Moneyness'
TIME = 'Time'
VOLATILITY = 'Volatility'
TARGET = ['Call Price']
FEATURES = ['Moneyness', 'Time', 'Volatility']