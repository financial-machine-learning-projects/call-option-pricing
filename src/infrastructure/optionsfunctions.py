import numpy as np
from scipy.stats import norm

import src.settings.base as stg


def option_vol_from_surface(moneyness, time_to_maturity):
    
    result = stg.true_sigma0 + stg.true_alpha * time_to_maturity +\
            stg.true_beta * np.square(moneyness - 1)
    
    return result


def call_option_price(moneyness, time_to_maturity, option_vol):
    
    d1 = (np.log(1/moneyness) + (stg.risk_free_rate + np.square(option_vol))*\
          time_to_maturity) / (option_vol * np.sqrt(time_to_maturity))
    
    d2 = (np.log(1/moneyness) + (stg.risk_free_rate - np.square(option_vol))*\
          time_to_maturity) / (option_vol * np.sqrt(time_to_maturity))
    
    N_d1 = norm.cdf(d1)
    
    N_d2 = norm.cdf(d2)
    
    return N_d1 - moneyness * np.exp(-stg.risk_free_rate*time_to_maturity) * N_d2