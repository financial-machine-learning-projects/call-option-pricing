import numpy as np
from scipy.stats import norm
import pandas as pd
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2, f_regression

from src.infrastructure.optionsfunctions import option_vol_from_surface, call_option_price
import src.settings.base as stg
from src.infrastructure.datageneration import CallData


def show_order_features():
    
    df = CallData().data
    X, Y = df[stg.FEATURES], df[stg.TARGET]
    
    bestfeatures = SelectKBest(k='all', score_func=f_regression)
    fit = bestfeatures.fit(X,Y)
    dfscores = pd.DataFrame(fit.scores_)
    dfcolumns = pd.DataFrame(['Moneyness', 'Time', 'Volatility'])

    #concat two dataframes for better visualization 
    featureScores = pd.concat([dfcolumns,dfscores],axis=1)
    featureScores.columns = ['Specs','Score']  #naming the dataframe columns
    featureScores.nlargest(10,'Score').set_index('Specs')  #print 10 best features
    
    return featureScores