import numpy as np
from scipy.stats import norm
import pandas as pd
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2, f_regression
from sklearn.neural_network import MLPRegressor

from src.infrastructure.optionsfunctions import option_vol_from_surface, call_option_price
import src.settings.base as stg
from src.infrastructure.datageneration import CallData

def main():
    
    # Create Dataset
    df = CallData().data
    X, Y = df[stg.FEATURES], df[stg.TARGET]
    
    
    validation_size = 0.2

    train_size = int(len(X) * (1-validation_size))
    X_train, X_test = X[0:train_size], X[train_size:len(X)]
    Y_train, Y_test = Y[0:train_size], Y[train_size:len(X)]
    
    mlpr = MLPRegressor(hidden_layer_sizes=(20, 30, 20))
    mlpr_fit = mlpr.fit(X_train, Y_train)
    
    return mlpr_fit.predict(X_test)